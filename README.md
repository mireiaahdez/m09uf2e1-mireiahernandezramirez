# M09UF2E1-MireiaHernandezRamirez

Recull d’informació sobre Threads
(Microsoft Threads)

· Pregunta 1:
Quins mètodes de la classe Thread trobes que pots necessitar si vols fer concurrència, que fan i en quin cas ho faries servir?

Per poder fer concurrència en la classe Thread, podem utilitzar els següents mètodes:
Start(): Per canviar de la instància actual a Running().
Interrupt(): Per interrompre un procès quan està al subprocès WaitSleepJoin().
Sleep(Int 32): Quan necessiti suspendre un subprocès en un temps determinat.
IsAlive(): Quan necessiti obtenir valor del subprocès actual.
Yield(): Per trucar a un altre subprocès per a que el procés actual li cedeixi la execució.
ThreadState(): Quan necessiti obtenir un valor amb els estats del subprocès actual.
Finalize(): Per finalitzar i alliurar recursos.

· Pregunta 2:
Esbrina què és la instrucció Yield i per a què pot ser útil en concurrència.

Yield() fa que el subprocés que realitza la trucada cedeixi l’execució a un altre subprocés el qual està llest per executar-se en el processador actual.
El SO selecciona el subprocés al que se li cedirà l’execució.

A la concurrència pot ser útil per combinar funcions juntament els seus resultats.


· Pregunta 3:
-Definicions:

Interface IEnumerator:
Defineix la manera de recórrer els elements d’una col·lecció. Conté una propietat anomenada “Actual” que retorna l’element actual de la col·lecció que és enumerat.

Interface IEnumerable:
Defineix un sol mètode GetEnumerator()  que retorna una interfaç IEnumerator.
GetEnumerator(): retorna el enumerador que itera a travès d’una col·lecció.


