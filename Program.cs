﻿using System;

public class Program
{

    /*https://learn.microsoft.com/es-es/dotnet/api/system.io.file.readlines?view=net-7.0 */

    static void CountLinesText()
	{
		int count = 0;

		foreach (string linestxt in System.IO.File.ReadLines(@"\Text.txt"){
			count++;
		}

		Console.Write("El text té :" + count + "linies.");
	}
}
